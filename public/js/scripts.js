$(function() {
    // navbar scroll
    $(window).on('scroll', function() {
        if ($(window).scrollTop()) {
            $('nav').addClass('black');
            $('nav').addClass('fixed');
            $('div.relleno').addClass('lleno');
            return false;
        } else {
            $('nav').removeClass('black');
            $('nav').removeClass('fixed');
            $('div.relleno').removeClass('lleno');
        }
    });


    // datedropper

});

$(document).ready(function() {
    const navSlide = () => {
        const burger = document.querySelector('.burger'),
            nav = document.querySelector('.nav-links'),
            navLinks = document.querySelectorAll('.nav-links li');
        burger.addEventListener('click', () => {
            // toggle nav
            nav.classList.toggle('nav-active');

            //Animate links
            navLinks.forEach((link, index) => {
                if (link.style.animation) {
                    link.style.animation = '';
                } else {
                    link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + .3}s`;
                }
            });
            // burger animation
            burger.classList.toggle('toggle');
        });

    }

    navSlide();
});