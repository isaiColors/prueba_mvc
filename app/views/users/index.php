<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Luxury</title>
    <link rel="stylesheet" href="<?= RUTAPUBLIC; ?>/css/normalize.css">
    <link rel="stylesheet" href="<?= RUTAPUBLIC; ?>/css/styles.css">
    

</head>

<body>
    <div class="hero">
        <nav class="flexi">
            <div class="logo">
                <img src="<?= RUTAPUBLIC; ?>/img/logosi.png" alt="Logo">
                <h4>Luxy</h4>
            </div>
            <ul class="nav-links">
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Work</a>
                </li>
                <li>
                    <a href="#">Projects</a>
                </li>
            </ul>
            <div class="burger">
                <div class="line1"></div>
                <div class="line2"></div>
                <div class="line3"></div>
            </div>
        </nav> <!-- .navbar -->
        <div class="relleno"></div>

        <div class="container search-container">
            <h2>Encuentra la casa de tus sueños</h2>
            <div class="announce">
                Tendrás la experiencia de tu vida
            </div>

            <form action="" method="post" class="search">
                <div class="content-form-search">
                    <div class="">
                        <select name="" id="" class="searcher">
                            <option value="" selected disabled>Zona</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                        </select>
                    </div>
                    <div class="">
                        <input type="text" class="searcher" data-datedropper placeholder="Llegada" data-dd-roundtrip="travel">
                    </div>
                    <div class="">
                        <input type="text" class="searcher" data-datedropper placeholder="Salida" data-dd-roundtrip="travel">
                    </div>
                    <div class="">
                        <input type="number" min="1" required class="searcher" placeholder="Adultos">
                    </div>
                    <div class="">
                        <input type="number" min="0" required class="searcher" placeholder="Niños">
                    </div>
                    <div class="">
                        <input type="submit" value="Search" class="send searcher">
                    </div>
                </div>
            </form>
        </div>
    </div> <!-- .banner -->

    <main class="container section">
        <h2>Recomendaciones</h2>
        <div class="border"></div>
        <div class="recommendations">
            <div class="card">
                <div class="card-image one"></div>
                <div class="card-text">
                    <span class="date greenT">4 days ago</span>
                    <h2>Post one</h2>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis, quibusdam at? </p>
                </div>
                <div class="card-stats greenB">
                    <div class="stat">
                        <div class="value">4<sup>m</sup></div>
                        <div class="type">read</div>
                    </div>
                    <div class="stat bordG">
                        <div class="value">5123</div>
                        <div class="type">views</div>
                    </div>
                    <div class="stat">
                        <div class="value">32</div>
                        <div class="type">comment</div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-image two"></div>
                <div class="card-text">
                    <span class="date orangeT">4 days ago</span>
                    <h2>Post two</h2>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis, quibusdam at? </p>
                </div>
                <div class="card-stats orangeB">
                    <div class="stat">
                        <div class="value">4<sup>m</sup></div>
                        <div class="type">read</div>
                    </div>
                    <div class="stat bordO">
                        <div class="value">5123</div>
                        <div class="type">views</div>
                    </div>
                    <div class="stat">
                        <div class="value">32</div>
                        <div class="type">comment</div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-image three"></div>
                <div class="card-text">
                    <span class="date pinkT">4 days ago</span>
                    <h2>Post three</h2>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis, quibusdam at? </p>
                </div>
                <div class="card-stats pinkB">
                    <div class="stat">
                        <div class="value">4<sup>m</sup></div>
                        <div class="type">read</div>
                    </div>
                    <div class="stat bordP">
                        <div class="value">5123</div>
                        <div class="type">views</div>
                    </div>
                    <div class="stat">
                        <div class="value">32</div>
                        <div class="type">comment</div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <div class="parallax all-houses">
        <div class="content-parallax container">
            <h1>Experiencias Increíbles</h1>
            <a href="houses.php">Ver Todas</a>
        </div>
    </div>

    <section class="testimonials">
        <div class="inner">
            <h2>Testimonials</h2>
            <div class="border"></div>
            <div class="row">
                <div class="col">
                    <div class="testimonial">
                        <img src="<?= RUTAPUBLIC; ?>/img/ts1.png" alt="Testimonial">
                        <div class="nameT">Full name</div>
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="far fa-star"></i>
                            <i class="far fa-star"></i>
                        </div>
                        <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae fuga, ab 
                        </p>
                    </div>
                </div>

                <div class="col">
                    <div class="testimonial">
                        <img src="<?= RUTAPUBLIC; ?>/img/p2.jpg" alt="Testimonial">
                        <div class="nameT">Full name</div>
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="far fa-star"></i>
                        </div>
                        <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae fuga, ab 
                        </p>
                    </div>
                </div>

                <div class="col">
                    <div class="testimonial">
                        <img src="<?= RUTAPUBLIC; ?>/img/p3.jpg" alt="Testimonial">
                        <div class="nameT">Full name</div>
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae fuga, ab 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="footer container flexi">
            <div class="leftF">
                <div class="logo">
                    <img src="<?= RUTAPUBLIC; ?>/img/logosi.png" alt="Logo">
                    <h4>Luxy</h4>
                </div>
                <div class="social-media">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                    <a href="#"><i class="fab fa-linkedin-in"></i></a>
                </div>
                <p class="rights-text"> &copy; 2020 Created by Divelopers All Rights Reserved.</p>
            </div>

            <div class="rightF">
                <h1>Our Newsletter</h1>
                <div class="borderF"></div>
                <p>Enter Your Email to get our news and updates.</p>
                <form action="" class="newslatterF flexi">
                    <input type="text" class="txtb" placeholder="Email">
                    <input type="submit" class="btnF" value="submit">
                </form>
            </div>
        </div>
    </footer>

    
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://kit.fontawesome.com/ee600140ae.js"></script>
    <script src="<?= RUTAPUBLIC; ?>/js/scripts.js"></script>
    <script src="<?= RUTAPUBLIC; ?>/js/datedropper.pro.min.js"></script>
</body>
</html>