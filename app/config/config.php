<?php
// base de datos
define("HOST", "localhost");
define("USER", "root");
define("PASS", "");
define("NAME", "lux");

// url app
define("RUTAAPP", dirname(dirname(__FILE__)));
// url public
define("RUTAPUBLIC", "http://localhost/git/luxemvc");

// nombre del sitio
define("NAMESITE", "Luxy");
